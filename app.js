
var express = require("express");
var exec = require("child_process").exec;
var config = require("./config.json");
var app = express();


app.use(express.bodyParser());
app.use(app.router);
app.use(express.logger());

app.get("/*", function(req, res) {
  return res.send("Nothing to see here. :)");
});

app.post("/", function(req, res) {
  
  var _ref;
  var payload = JSON.parse(req.body.payload);
  var bburl = payload.canon_url + payload.repository.absolute_url;
  var localpath = (_ref = config[bburl]) != null ? _ref.localpath : void 0;

  if (localpath && payload.commits.count(function(commit) {
    return /#deploy/.test(commit.message);
  })) {
    return exec("git pull", {
      cwd: localpath
    }, function(err, stdout, stderr) {
      if (err) {
        return res.send("Deployment failed");
      } else {
        return res.send("ok");
      }
    });
  }
});

app.listen(9001, "127.0.0.1");