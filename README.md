# Deployment Automation Service for BitBucket

deploy-bitbucket is a web service which accepts POST requests from BitBucket hooks and updates 
local repositories. 

# Installation

* Clone deploy-bitbucket from `git clone https://bitbucket.org/sergeibelov/deploy-bitbucket.git`
* Install dependencies: `npm install`
* Rename `config.sample.json` to `config.json` and edit configuration
* Launch the service
* Create a BitBucket hook to POST to the newly created service URL

# Usage

Add `#deploy` to the commit message and push changes to your BitBucket repository.